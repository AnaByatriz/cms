package Beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

import Controle.ImagemC;
import Modelo.ImagemM;

/**
 * 
 * @author Bya Lopes
 *
 */
@ManagedBean(name="ImgBean")

public class ImgBean {
	private ArrayList<ImagemM> lista = new ImagemC().consultarTodos();
	private int id;
	private Part imagem;
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	
	
	public String pegarImagem() throws IOException {
		ImagemM mod = new ImagemM();
		ImagemC con = new ImagemC();
		String pasta = "C:\\Users\\Bya Lopes\\Desktop\\eclipse\\trabalho\\cms\\WebContent\\imagem";
		InputStream imag = imagem.getInputStream();
		String nome = imagem.getSubmittedFileName();
		Files.copy(imag, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretImg = "imagem/"+""+nome;
		mod.setImagem(diretImg);
		con.inserirImagem(mod);
		return "imagem";
	}
	
	public ArrayList<ImagemM> getLista() {
		return lista;
	}	
	public void setLista(ArrayList<ImagemM> lista) {
		this.lista = lista;
	}		
	
	public ImagemM retornarItem(int id) {
		return lista.get(id);
	}
		
	public String deletar(int id) {
		new ImagemC().remover(id);
		return "AdicionarImg.xhtml";
	}
}
