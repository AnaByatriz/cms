package Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import Modelo.UsuarioM;
import Controle.UsuarioC;

@ManagedBean(name="UserAdd")
@RequestScoped

public class UserAdd {
	int id;
	String nome;
	String senha;	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}	
	public String add() {
		UsuarioM user = new UsuarioM(this.getId(),this.getNome(), this.getSenha());
		if(new UsuarioC().adicionar(user)) {
			return "index";
		}else {
			return "AdicionarUser";
		}
	}
}
