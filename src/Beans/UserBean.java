package Beans;

import java.security.SecureRandom;
import java.util.ArrayList;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import java.util.Base64;
import criptografia.MyCrypto;
import Controle.UsuarioC;
import Modelo.UsuarioM;

@ManagedBean(name="UserBean")
public class UserBean {
	private ArrayList<UsuarioM> lista = new UsuarioC().consultarUsuario();
	private int id;
	private String nome;
	private String senha;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
	public boolean adicionar(){		
		boolean resultado = false;
		UsuarioM model = new UsuarioM();
		UsuarioC control = new UsuarioC();
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        SecretKey key = keyGenerator.generateKey();
        byte[] IV = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);
        byte[] emailCripto = MyCrypto.encrypt(nome.getBytes(),key, IV);
        String cpE = Base64.getEncoder().encodeToString(emailCripto);
        byte[] senhaCripto = MyCrypto.encrypt(senha.getBytes(),key, IV);
        String cpS = Base64.getEncoder().encodeToString(senhaCripto);
        String iv = Base64.getEncoder().encodeToString(IV);
        String kee = Base64.getEncoder().encodeToString(key.getEncoded());
        model.setNome(cpE);
        model.setSenha(cpS);
        model.setIv(iv);
        model.setKeey(kee);	
        control.inserir(model); 	
		resultado = true;
	}catch(Exception e) {
		e.getMessage();
	}
	return resultado;
	}
	
	public boolean verificar() {
		boolean resultado = false;
				UsuarioM crip = new UsuarioM();
				UsuarioC control = new UsuarioC();
				try {	
					crip = control.consultaID(1);
			        byte[] tt = Base64.getDecoder().decode(crip.getNome());
			        byte[] t1 = Base64.getDecoder().decode(crip.getSenha());
			        byte[] key = Base64.getDecoder().decode(crip.getKeey());
			        byte[] iv = Base64.getDecoder().decode(crip.getIv());
			        SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");
			        System.out.println("teste"+tt+"-"+t1);
					String nomeD = MyCrypto.decrypt(tt,originalKey, iv);
			        String senhaD = MyCrypto.decrypt(t1,originalKey, iv);
			        System.out.println("Email : "+nomeD+"-"+nome);
			        System.out.println("Senha : "+senhaD+"-"+senha);
			        if(nome.equals(nomeD) && senha.equals(senhaD)) {
			        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login", nomeD);
			        System.out.println("Sess�o>>>"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login"));  
			        FacesContext.getCurrentInstance().getExternalContext().redirect("Index.xhtml");
			        }else {
			        	System.out.println("Erro>>>"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login"));  
				        FacesContext.getCurrentInstance().getExternalContext().redirect("AdicionarUser.xhtml");
			        }
					resultado = true;
				} catch (Exception e) {
					e.printStackTrace();
				}				
		return resultado;
	}

	public ArrayList<UsuarioM> getLista() {
		return lista;
	}
	public void setLista(ArrayList<UsuarioM> lista) {
		this.lista = lista;
	}
	public UsuarioM retornarItem(int id) {
		return lista.get(id);
	}	

	

}
