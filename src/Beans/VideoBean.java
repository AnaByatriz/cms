package Beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

import Controle.VideoC;
import Modelo.VideoM;

@ManagedBean(name="VideoBean")
public class VideoBean {
	private ArrayList<VideoM> lista = new VideoC().consultarTodos();
	private int id;
	private Part video;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getVideo() {
		return video;
	}
	public void setVideo(Part video) {
		this.video = video;
	}

	public String pegarVideo() throws IOException {
		VideoM mod = new VideoM();
		VideoC con = new VideoC();
		String pasta = "C:\\Users\\Bya Lopes\\Desktop\\eclipse\\trabalho\\cms\\WebContent\\video";
		InputStream vid = video.getInputStream();
		String nome = video.getSubmittedFileName();
		Files.copy(vid, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretVid = "video/"+""+nome;
		mod.setVideo(diretVid);
		con.inserirVideo(mod);
			return "video";
	}	
		
		public ArrayList<VideoM> getLista() {
			return lista;
		}	
		public void setLista(ArrayList<VideoM> lista) {
			this.lista = lista;
		}		
		
		public VideoM retornarItem(int id) {
			return lista.get(id);
		}
			
		public String deletar(int id) {
			new VideoC().remover(id);
			return "AdicionarVid.xhtml";
		}
	}


