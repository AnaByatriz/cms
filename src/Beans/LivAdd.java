package Beans;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import Modelo.LivroM;
import Controle.LivroC;

@ManagedBean(name="LivAdd")
@RequestScoped

public class LivAdd {
	int id;
	int paginas;
	String nomeLivro;
	String categoria;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPaginas() {
		return paginas;
	}
	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}
	public String getNomeLivro() {
		return nomeLivro;
	}
	public void setNomeLivro(String nomeLivro) {
		this.nomeLivro = nomeLivro;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String add() {
		LivroM liv = new LivroM(this.getId(), this.getPaginas(), this.getNomeLivro(), this.getCategoria()); 
		if(new LivroC().adicionar(liv)) {
			return "index";
		}else {
			return "AddLivro";
		}
	}
}
