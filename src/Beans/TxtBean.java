/**
 * 
 */
package Beans;

import java.util.ArrayList; 
import javax.faces.bean.ManagedBean;
import Controle.TextoC;
import Modelo.TextoM;

/**
 * @author Bya Lopes
 *
 */

@ManagedBean(name="TxtBean")
public class TxtBean {
	private ArrayList<TextoM> lista = new TextoC().consultarTodos();
	private int id;
	private String texto;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public String adicionar() {
		TextoM txt= new TextoM();
		TextoC con = new TextoC();
		try {
			txt.setId(this.id);
			txt.setTexto(this.texto);
			con.adicionar(txt);
			
		}catch(Exception e) {
			e.getMessage();
			
		}
		return "AdicionarTxt";
	}
	
	public ArrayList<TextoM> getLista() {
		return lista;
	}
	public void setLista(ArrayList<TextoM> lista) {
		this.lista = lista;
	}
	
	public TextoM retornarItem(int id) {
		return lista.get(id);
	}	
	
	public String editar() {
		TextoM txt= new TextoM();
		TextoC con = new TextoC();
		txt.setId(id);
		txt.setTexto(texto);
		if(con.atualizar(txt)) {
			return "AdicionarTxt.xhtml";
		}else {
			return "EditarTxt.xhtml";
		}
	}
	
	public String remover(int id) {
		new TextoC().remover(id);
		return "AdicionarTxt.xhtml";
	}
	
	public void carregarId(int id) {
		TextoM con = new TextoC().consultar(id);
		this.setId(con.getId());
		this.setTexto(con.getTexto());
	}

}
