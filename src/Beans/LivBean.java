package Beans;

import java.util.ArrayList; 
import javax.faces.bean.ManagedBean;
import Controle.LivroC;
import Modelo.LivroM;

@ManagedBean(name="LivBean")
public class LivBean {
	private ArrayList<LivroM> lista = new LivroC().consultarTodos();
	private int id;
	private int paginas;
	private String nomeLivro;
	private String descricao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPaginas() {
		return paginas;
	}
	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}
	public String getNomeLivro() {
		return nomeLivro;
	}
	public void setNomeLivro(String nomeLivro) {
		this.nomeLivro = nomeLivro;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String adicionar() {
		LivroM liv = new LivroM();
		LivroC con = new LivroC();
		try {
			liv.setId(this.id);
			liv.setPaginas(this.paginas);
			liv.setNomeLivro(this.nomeLivro);
			liv.setDescricao(this.descricao);
			con.adicionar(liv);
			
		}catch(Exception e) {
			e.getMessage();
			
		}
		return "AdicionarLiv";
	}
	
	public ArrayList<LivroM> getLista() {
		return lista;
	}
	public void setLista(ArrayList<LivroM> lista) {
		this.lista = lista;
	}
	public LivroM retornarItem(int id) {
		return lista.get(id);
	}	
	
	public String editar() {
		LivroM liv = new LivroM();
		LivroC con = new LivroC();
		liv.setId(id);
		liv.setPaginas(paginas);
		liv.setNomeLivro(nomeLivro);
		liv.setDescricao(descricao);
		if(con.atualizar(liv)) {
			return "AdicionarLiv.xhtml";
		}else {
			return "EditarLiv.xhtml";
		}
	}
	
	public String remover(int id) {
		new LivroC().remover(id);
		return "AdicionarLiv.xhtml";
	}
	
	public void carregarId(int id) {
		LivroM con = new LivroC().consultar(id);
		this.setId(con.getId());
		this.setPaginas(con.getPaginas());
		this.setNomeLivro(con.getNomeLivro());
		this.setDescricao(con.getDescricao());
	}
}
