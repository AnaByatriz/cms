package Beans;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
@ManagedBean(name="Deslogar")
@RequestScoped
@SessionScoped
public class Deslogar {
	public boolean deslogar() {
		boolean a = true;
		if(FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login") != null) {
	    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("login");
	    try {
	    	System.out.println("Sucesso>>>"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login")); 
			FacesContext.getCurrentInstance().getExternalContext().redirect("Index.xhtml"); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	    return a;
	}
}
