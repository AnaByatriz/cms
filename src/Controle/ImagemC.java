package Controle;

import Modelo.ImagemM;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * 
 * @author Bya Lopes
 *
 */
public class ImagemC {
	public ImagemM consultar(int id){
		ImagemM img = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem WHERE id=?;");
			ps.setInt(1,id);
			ResultSet rs = ps.executeQuery();
			if(rs != null){
				img = new ImagemM();
				img.setId(rs.getInt("id"));
				img.setImagem(rs.getString("imagem"));
				new Conexao().fecharConexao(con);				
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return img;
	}
	
	public ArrayList<ImagemM> consultarTodos(){
		ArrayList<ImagemM> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem;");
			ResultSet rs = ps.executeQuery();
			if(rs != null){
				lista = new ArrayList<ImagemM>();
				while(rs.next()){
					ImagemM img = new ImagemM();
					img.setImagem(rs.getString("imagem"));
					img.setId(rs.getInt("id"));
					lista.add(img);
				}
			}	
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean inserirImagem(ImagemM imagem) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("INSERT INTO imagem(imagem) VALUES(?);");
			ps.setString(1, imagem.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}	
			new Conexao().fecharConexao(con);
		}catch(SQLException e ) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean remover(int id){
		boolean resultado= false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM imagem WHERE id=?;");
			ps.setInt(1, id);
			if(!ps.execute()){
				resultado = true;
			}					
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao deletar: " + e.getMessage());
		}
		return resultado;
	}
	
	public boolean atualizar(ImagemM imagem){
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE imagem SET imagem=?  WHERE id=?");
			ps.setString(1, imagem.getImagem());
			ps.setInt(2, imagem.getId());
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar: " + e.getMessage());
		}
	return resultado;
}
}	