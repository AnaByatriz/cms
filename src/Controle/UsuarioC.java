package Controle;
import Modelo.UsuarioM;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UsuarioC {
					
	public ArrayList<UsuarioM> consultarUsuario(){
		ArrayList<UsuarioM> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario1;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<UsuarioM>();
				while(rs.next()) {
					UsuarioM user = new UsuarioM();
					user.setId(rs.getInt("id"));
					user.setNome(rs.getString("nome"));
					user.setSenha(rs.getString("senha"));
					user.setIv(rs.getString("iv"));
					user.setKeey(rs.getString("keey"));
					lista.add(user);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean inserir(UsuarioM cripto) throws SQLException{
		boolean resultado = false;	
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO usuario1(nome,senha,iv,keey) VALUES(?,?,?,?);";	
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, cripto.getNome());
			ps.setString(2, cripto.getSenha());
			ps.setString(3, cripto.getIv());
			ps.setString(4, cripto.getKeey());
			if(!ps.execute()) {
				resultado = true;		
				new Conexao().fecharConexao(con);		
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	
	public UsuarioM consultaID(int n) throws SQLException{
		UsuarioM a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM usuario1 where id= ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new UsuarioM();
				while(rs.next()) {
					a.setId(rs.getInt("id"));
					a.setNome(rs.getString("nome"));
					a.setSenha(rs.getString("senha"));
					a.setIv(rs.getString("iv"));
					a.setKeey(rs.getString("keey"));
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao Consultar.");
			}
		return a;
	}

	}
