package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Modelo.TextoM;

public class TextoC {
	public boolean adicionar(TextoM txt) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps =con.prepareStatement("INSERT INTO texto(texto) VALUES(?);");
			ps.setString(1, txt.getTexto());
			if(!ps.execute()) {
				resultado = true; 
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar. " + e.getMessage());
		}
		return resultado;
	}
	
	public boolean atualizar(TextoM txt){
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE texto SET texto=? WHERE id=?");
			ps.setInt(2, txt.getId());
			ps.setString(1, txt.getTexto());
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar " + e.getMessage());
		}
	return resultado;
	}
	
	public ArrayList<TextoM> consultarTodos(){
		ArrayList<TextoM> lista = null;
		try{
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM texto ;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<TextoM>();
				while(rs.next()) {
					TextoM txt = new TextoM();
					txt.setId(rs.getInt("id"));
					txt.setTexto(rs.getString("texto"));
					lista.add(txt);
				}
			}
			new Conexao().fecharConexao(con);		
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM texto WHERE id=?;");
			ps.setInt(1, id);
			if(ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao deletar " + e.getMessage());
		}
		return resultado;
	}
	
	public TextoM consultar(int id) {
		TextoM txt = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement(" SELECT * FROM texto WHERE id=? ");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				rs.next();
				txt = new TextoM();
				txt.setId(rs.getInt("id"));
				txt.setTexto(rs.getString("texto"));
				new Conexao().fecharConexao(con);
			}
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return txt;
	}
}
