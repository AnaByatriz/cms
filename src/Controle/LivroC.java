package Controle;
import Modelo.LivroM;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

public class LivroC {
	
	public boolean adicionar(LivroM liv) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps =con.prepareStatement("INSERT INTO livro(paginas,nomeLivro,descricao) VALUES(?,?,?);");
			ps.setInt(1, liv.getPaginas());
			ps.setString(2, liv.getNomeLivro());
			ps.setString(3, liv.getDescricao());
			if(!ps.execute()) {
				resultado = true; 
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar. " + e.getMessage());
		}
		return resultado;
	}
	
	public boolean atualizar(LivroM liv){
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE livro SET paginas=?, nomeLivro=?, descricao=? WHERE id=?");
			ps.setInt(4, liv.getId());
			ps.setInt(1, liv.getPaginas());
			ps.setString(2, liv.getNomeLivro());
			ps.setString(3, liv.getDescricao());
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar " + e.getMessage());
		}
	return resultado;
	}
	
	public ArrayList<LivroM> consultarTodos(){
		ArrayList<LivroM> lista = null;
		try{
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM livro;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<LivroM>();
				while(rs.next()) {
					LivroM liv = new LivroM();
					liv.setId(rs.getInt("id"));
					liv.setPaginas(rs.getInt("paginas"));
					liv.setNomeLivro(rs.getString("nomeLivro"));
					liv.setDescricao(rs.getString("descricao"));
					lista.add(liv);
				}
			}
			new Conexao().fecharConexao(con);		
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM livro WHERE id=?;");
			ps.setInt(1, id);
			if(ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao deletar " + e.getMessage());
		}
		return resultado;
	}
	
	public LivroM consultar(int id) {
		LivroM liv = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement(" SELECT * FROM livro WHERE id=? ");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				rs.next();
				liv = new LivroM();
				liv.setId(rs.getInt("id"));
				liv.setPaginas(rs.getInt("paginas"));
				liv.setNomeLivro(rs.getString("nomeLivro"));
				liv.setDescricao(rs.getString("descricao"));
				new Conexao().fecharConexao(con);
			}
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return liv;
	}
}

