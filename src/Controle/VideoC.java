package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.VideoM;

public class VideoC {
		
	public ArrayList<VideoM> consultarTodos(){
		ArrayList<VideoM> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM video;");
			ResultSet rs = ps.executeQuery();
			if(rs != null){
				lista = new ArrayList<VideoM>();
				while(rs.next()){
					VideoM vid = new VideoM();
					vid.setVideo(rs.getString("video"));
					vid.setId(rs.getInt("id"));
					lista.add(vid);
				}
			}	
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean inserirVideo(VideoM video) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("INSERT INTO video(video) VALUES(?);");
			ps.setString(1, video.getVideo());
			if(!ps.execute()) {
				resultado = true;
			}	
			new Conexao().fecharConexao(con);
		}catch(SQLException e ) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean remover(int id){
		boolean resultado= false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM video WHERE id=?;");
			ps.setInt(1, id);
			if(!ps.execute()){
				resultado = true;
			}					
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao deletar: " + e.getMessage());
		}
		return resultado;
	}
	
	
}
